@extends('layouts.master')

@section('content')
<main class="container">
    <div class="row">
        <h3 class="pull-right">Orçamento</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-6 text-left">
                    <address>
                    <strong>Cliente:</strong><br>
                        {{ $invoice['cliente'] }}<br>
                        {{ $invoice['cpf_cnpj'] }}<br>
                        {{ $invoice['fazenda'] }}<br>
                        {{ $invoice['contato'] }}
                    </address>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>Método de Pagamento:</strong><br>
                        {{ $invoice['formPag'] }}<br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Data do Orçamento:</strong><br>
                        {{ $invoice['data'] }}<br><br>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3><strong>Resumo do Orçamento</strong></h3>
            <table class="table table-bordered">
                <tbody>
                 @foreach ($invoice['produtos'] as $produto)
                    <tr>
                        <td class="text-left">
                            <span><strong>Produto: </strong>{{ $produto['title'] }}</span><br>
                            <span><strong>Quantidade: </strong>{{ $produto['quant'] }}</span><br>
                            <span><strong>Preço Unitário: </strong>{{ $produto['valor'] }}</span><br>
                            <span><strong>Subtotal: </strong>{{ $produto['subTot'] }}</span><br>
                        </td>
                        <td class="text-center col-xs-6">
                            <img src="{{ $produto['image'] }}" alt="" class="img-fluid img-thumbnail">
                        </td>
                    </tr>
                 @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>
@endsection
