<?php

namespace App\Http\Middleware;

use Closure;

class CheckForAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->get('x-access-token') ?? $request->headers->get('x-access-token');

        if (!$token) {
            return response()->json([
                'message' => 'Invalid auth token!'
            ], 401);
        }

        if (!$token === config('app.auth.token')) {
            return response()->json([
                'message' => 'Invalid auth token!'
            ], 401);
        }

        return $next($request);
    }
}
