<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use VerumConsilium\Browsershot\Facades\PDF;

class InvoicePdfController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        try {
            $invoice = $request->get('invoice');

            $header = view('invoice.header')->render();
            $footer = view('invoice.footer')->render();

            $filename = md5(uniqid(rand(), true)) . ".pdf";
            PDF::loadView('invoice.pdf', [
                'invoice' => $invoice,
            ])
                ->showBrowserHeaderAndFooter()
                ->headerHtml($header)
                ->footerHtml($footer)
                ->emulateMedia('screen')
                ->waitUntilNetworkIdle()
                ->showBackground()
                ->fullPage()
                ->setOption('format', 'A4')
                ->noSandbox()
                ->margins(170, 30, 80, 30, 'px')
                ->storeAs('pdfs/', $filename);

            return response()->json([
                'message' => 'PDF invoice generated with success!',
                'url' => route('invoice.show', $filename),
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param $filename
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function show($filename)
    {
        try {
            $file = storage_path('app/pdfs/' . $filename);

            return response()->download($file, "$filename.pdf", [], 'inline');
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
