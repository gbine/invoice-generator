<?php

use Illuminate\Http\Request;
use App\Http\Middleware\CheckForAuthToken;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'middleware' => CheckForAuthToken::class,
    'namespace' => 'Api\v1',
], function () {
    Route::get('/pdf/{filename}', 'InvoicePdfController@show')->name('invoice.show');
    Route::post('/pdf', 'InvoicePdfController@store')->name('invoice.store');
});
