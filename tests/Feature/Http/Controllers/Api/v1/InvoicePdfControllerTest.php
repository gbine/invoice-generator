<?php

namespace Tests\Feature\Http\Controllers\Api\v1;

use Tests\TestCase;

class InvoicePdfControllerTest extends TestCase
{
    private $invoiceMock = [
        'cliente' => 'Daniel Lucas',
        'cpf_cnpj' => '088.463.559-70',
        'createdBy' => 'createdBy',
        'data' => '2019-08-18',
        'endereco' => 'LARANJEIRAS DO SUL PR',
        'fazenda' => 'Fazenda Jao Martins',
        'id' => '70cb105e-493e-1041-c81d-1a13cc0eddcb',
        'formPag' => 'Dinheiro',
        'pedidoCheck' => true,
        'contato' => '042991044320',
        'produtos' => [
            [
                '_id' => '5b80b68cb9d735001471baef',
                'family' => 'http://romancini.com.br/ftp/fotos/13.jpg',
                'quant' => 2,
                'title' => 'TRONCO TRADICIONAL 02P CC',
                'valor' => 500,
            ]
        ],
        'valor' => 1000,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /** @test */
    public function it_can_create_a_pdf_invoice_with_params_and_return_a_filename()
    {
        $response = $this->post(
            '/api/v1/pdf',
            $this->invoiceMock,
            [
                'x-access-token' => 'token',
            ]
        );

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'message',
            'filename',
        ]);
    }
}
